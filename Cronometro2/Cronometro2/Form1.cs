﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Cronometro2
{
    public partial class Form1 : Form
    {
        Stopwatch Cro;
        bool stat = true;
        public Form1()
        {
            InitializeComponent();
            Cro = new Stopwatch();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Enabled = true;
            button1.Enabled = true;
        }

        private void timerT_Tick(object sender, EventArgs e)
        {
            if (stat)
            {
                TimeSpan de = Cro.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}:{3:00}", de.Hours, de.Minutes, de.Seconds, de.Milliseconds / 10);
                label1.Text = elapsedTime;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (stat)
            {
                timerT.Enabled = true;
                Cro.Start();
                button1.Enabled = false;
                button3.Enabled = false;
                button2.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            stat = false;
            if (!stat)
            {
                Cro.Stop();
                button1.Enabled = true;
                button3.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TimeSpan de = TimeSpan.Zero;
            //string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}:{3:00}", de.Hours, de.Minutes, de.Seconds, de.Milliseconds / 10);
            label1.Text = de.ToString();
            timerT.Enabled = false;
            stat = true;
        }
    }
}
